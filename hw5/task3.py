# 3. Создать текстовый файл (не программно), построчно записать фамилии сотрудников и величину их окладов (не менее 10 строк). 
# Определить, кто из сотрудников имеет оклад менее 20 тыс., вывести фамилии этих сотрудников. 
# Выполнить подсчет средней величины дохода сотрудников.

# Пример файла:

# Иванов 23543.12
# Петров 13749.32

import os
from helpers import getCurrentDirectoryFilePath

file_path = getCurrentDirectoryFilePath('task3.txt')


def createFile():
    with open(file_path, 'w') as fl:
        print('Ivanov 18123.00', file=fl)
        print('Petrov 19123.01', file=fl)
        print('Sidorov 22123.02', file=fl)
        print('Egorov 23123.03', file=fl)
        print('Dmitriev 24123.23', file=fl)
        print('Vasiliev 25123.54', file=fl)
        print('Vlasov 26123.10', file=fl)
        print('Andreev 27123.32', file=fl)
        print('Sergeev 28123.12', file=fl)
        print('Antonov 29123.11', file=fl)


def getEmployee() -> dict:
    if not os.path.exists(file_path):
        print('File was not found')
        return

    emps = dict()

    with open(file_path) as fl:
        for line in fl.readlines():
            e = line.split()
            emps.__setitem__(e[0], float(e[1]))
    
    return emps


def getEmployeeLessTwentyK(emps:dict):
    for k, v in emps.items():
        if v < 20000:
            print(k, v)


def getAverageSalary(emps:dict):
    total = 0
    
    for v in emps.values():
        total += v
    
    print(total / len(emps))


createFile()
employee = getEmployee()
getEmployeeLessTwentyK(employee)
getAverageSalary(employee)