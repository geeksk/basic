# 2. Создать текстовый файл (не программно), сохранить в нем несколько строк, выполнить подсчет количества строк, количества слов в каждой строке.

import os
from helpers import getCurrentDirectoryFilePath


file_name = 'task2.txt'
file_path = getCurrentDirectoryFilePath(file_name)


def createFile():
    with open(file_path, 'w') as fl:
        for i in range(0, 6):
            print("Just a text to calculate lines and words", file=fl)


def processFile():
    result = { 
        'lines': 0,
        'words': {}
        }

    if not os.path.exists(file_path):
        print('File was not found')
        return

    with open(file_path) as fl:
        for line in fl.readlines():
            result['lines'] += 1
            result['words'].__setitem__('line {0}'.format(result['lines']), len(line.split()))

    for i in result.items():
        print(i)


createFile()
processFile()