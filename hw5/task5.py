# 5. Создать (программно) текстовый файл, записать в него программно набор чисел, разделенных пробелами. 
# Программа должна подсчитывать сумму чисел в файле и выводить ее на экран.

import os, random
from helpers import getCurrentDirectoryFilePath

file_name = 'task5.txt'
file_path = getCurrentDirectoryFilePath(file_name)


def createFile():
    with open(file_path, 'w') as fl:
        for i in range(0, 10):
            fl.write('{0} '.format(random.randint(1, 101)))


def readFile():
    if not os.path.exists(file_path):
        print('File was not found')
        return

    result = 0

    with open(file_path) as fl:
        data = fl.read().split()
    
    for it in data:
        result += int(it)
    
    print(result)


createFile()
readFile()