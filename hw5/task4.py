# 4. Создать (не программно) текстовый файл со следующим содержимым:

# One — 1
# Two — 2
# Three — 3
# Four — 4
# Необходимо написать программу, открывающую файл на чтение и считывающую построчно данные. 
# При этом английские числительные должны заменяться на русские. 
# Новый блок строк должен записываться в новый текстовый файл.

import os
from helpers import getCurrentDirectoryFilePath

file_name_en = 'task4-en.txt'
file_name_ru = 'task4-ru.txt'
file_path_en = getCurrentDirectoryFilePath(file_name_en)
file_path_ru = getCurrentDirectoryFilePath(file_name_ru)

dictionary = {
    'One': 'один',
    'Two': 'два',
    'Three': 'три',
    'Four': 'четыре'
}

def createFile():
    with open(file_path_en, 'w') as fl:
        print('One — 1', file=fl)
        print('Two — 2', file=fl)
        print('Three — 3', file=fl)
        print('Four — 4', file=fl)


def createFileRu(data:list):
    with open(file_path_ru, 'w') as fl:
        for line in data:
            print(line, file=fl)


def processFile():
    if not os.path.exists(file_path_en):
        print('File was not found')
        return
    
    result = list()

    with open(file_path_en) as fl:
        for line in fl.readlines():
            line = line.split()
            line[0] = dictionary[line[0]]
            result.append(' '.join(line))

    return result


createFile()
createFileRu(processFile())