import sys, os


def clearScr():
    os.system('cls' if os.name == 'nt' else 'clear')


def getCurrentDirectoryFilePath(file_name):
    dir_path = os.path.dirname(os.path.realpath(__file__)) # getting directory where python script is
    return os.path.join(dir_path, file_name)