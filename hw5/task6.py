# 6. Необходимо создать (не программно) текстовый файл, где каждая строка описывает учебный предмет и 
# наличие лекционных, практических и лабораторных занятий по этому предмету и их количество. 
# Важно, чтобы для каждого предмета не обязательно были все типы занятий. 
# Сформировать словарь, содержащий название предмета и общее количество занятий по нему. 
# Вывести словарь на экран.

# Примеры строк файла: 
# Информатика: 100(л) 50(пр) 20(лаб).
# Физика: 30(л) — 10(лаб)
# Физкультура: — 30(пр) —

# Пример словаря: {“Информатика”: 170, “Физика”: 40, “Физкультура”: 30}
import os, re
from helpers import getCurrentDirectoryFilePath

file_path = getCurrentDirectoryFilePath('task6.txt')


def createFile():
    with open(file_path, 'w') as fl:
        print('Информатика: 100(л) 50(пр) 20(лаб)', file=fl)
        print('Физика: 30(л) — 10(лаб)', file=fl)
        print('Физкультура: — 30(пр) —', file=fl)


def readFile():
    result = dict()
    with open(file_path) as fl:
        for line in fl.readlines():
            subj = line.split()
            amount = 0
            for t in subj[1:]:
                digits = re.findall(r'\d+', t)
                if len(digits) > 0:
                    amount += int(digits[0])
            result.update({subj[0]: amount})
    print(result)


createFile()
readFile()