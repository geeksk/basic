# 7. Создать вручную и заполнить несколькими строками текстовый файл, в котором каждая 
# строка должна содержать данные о фирме: название, форма собственности, выручка, издержки.

# Пример строки файла: firm_1 ООО 10000 5000.

# Необходимо построчно прочитать файл, вычислить прибыль каждой компании, а также среднюю прибыль. 
# Если фирма получила убытки, в расчет средней прибыли ее не включать.
# Далее реализовать список. 
# Он должен содержать словарь с фирмами и их прибылями, а также словарь со средней прибылью. 
# Если фирма получила убытки, также добавить ее в словарь (со значением убытков).

# Пример списка: [{“firm_1”: 5000, “firm_2”: 3000, “firm_3”: 1000}, {“average_profit”: 2000}].
# Итоговый список сохранить в виде json-объекта в соответствующий файл.

# Пример json-объекта:

# [{"firm_1": 5000, "firm_2": 3000, "firm_3": 1000}, {"average_profit": 2000}]

# Подсказка: использовать менеджер контекста.

import os, re, json
from helpers import getCurrentDirectoryFilePath

file_path = getCurrentDirectoryFilePath('task7.txt')
positive = {}
negative = {}


def createFile():
    with open(file_path, 'w') as fl:
        print('firm_1 ООО 10000 5000', file=fl)
        print('firm_2 AAA 20000 15000', file=fl)
        print('firm_3 OAO 12000 15000', file=fl)
        print('firm_4 OPG 32000 15000', file=fl)
        print('firm_5 OAO 18000 15000', file=fl)
        print('firm_6 OOO 22000 25000', file=fl)


def processFirm(firm:list):
    r = int(firm[2]) - int(firm[3])
    
    if r > 0:
        positive[firm[0]] = r
    else:
        negative[firm[0]] = r


def getAverage(firms:dict):
    total = 0
    for v in firms.values():
        total += v
        print(total)
    return total / len(firms)


def readFile():
    with open(file_path) as fl:
        for line in fl.readlines():
            firm = line.split()
            processFirm(firm)


def writeFile():
    data = [positive, {'average_profit': getAverage(positive)}]
    json_file = getCurrentDirectoryFilePath('profit.json')
    with open(json_file, 'w') as fl:
        json.dump(data, fl)


createFile()
readFile()
writeFile()