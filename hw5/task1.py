# 1. Создать программно файл в текстовом формате, записать в него построчно данные, вводимые пользователем. 
# Об окончании ввода данных свидетельствует пустая строка.
import sys, os
import helpers as h

file_name = 'task1.txt'
file_path = h.getCurrentDirectoryFilePath(file_name)


def writeToFile(text:str):
    f_mode = 'a' if os.path.exists(file_path) else 'w'
    with open(file_path, f_mode) as fl:
        print(text, file=fl)


def inputText():
    user_input = ''
    while True:
        h.clearScr()
        print('Enter text to write to a file')
        user_input = input('>> ')
        if user_input == '':
            return
        writeToFile(user_input)


def printFile():
    h.clearScr()
    if os.path.exists(file_path):
        with open(file_path) as fl:
            for l in fl.readlines():
                print(l, end='')


inputText()
printFile()
exit()