# 1. Создать класс TrafficLight (светофор) и определить у него один атрибут color (цвет) и метод running (запуск).
#   Атрибут реализовать как приватный.
#   В рамках метода реализовать переключение светофора в режимы: красный, желтый, зеленый.
#   Продолжительность первого состояния (красный) составляет 7 секунд, второго (желтый) — 2 секунды, третьего (зеленый) — на ваше усмотрение.
#   Переключение между режимами должно осуществляться только в указанном порядке (красный, желтый, зеленый).
#   Проверить работу примера, создав экземпляр и вызвав описанный метод.

# Задачу можно усложнить, реализовав проверку порядка режимов, и при его нарушении выводить соответствующее сообщение и завершать скрипт.
import os
from threading import Timer


class TrafficLight:
    color: str
    __lights: dict
    __curr_ind: int

    def __init__(self):
        self.__curr_ind = 0
        self.__lights = [
            {"name": "RED", "sec": 7},
            {"name": "YELLOW", "sec": 2},
            {"name": "GREEN", "sec": 5}
        ]

    def running(self):
        self.__clear_screen()
        self.__show_color()
        t = Timer(self.__lights[self.__curr_ind]["sec"], self.__next_color)
        t.start()

    def __show_color(self):
        self.__curr_ind = self.__curr_ind if self.__curr_ind < len(self.__lights) else 0
        self.color = self.__lights[self.__curr_ind]["name"]
        print(self.color)

    def __next_color(self):
        self.__curr_ind += 1
        self.running()

    def __clear_screen(self):
        os.system('cls' if os.name == 'nt' else 'clear')


tr_lights = TrafficLight()
tr_lights.running()
