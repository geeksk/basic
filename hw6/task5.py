# 5. Создайте экземпляры классов, передайте значения атрибутов. 
#   Выполните доступ к атрибутам, выведите результат. 
#   Выполните вызов методов и также покажите результат.

class A:
    prop1: int
    __prop2: str

    def __init__(self, prop1: int, prop2: str):
        self.prop1 = prop1
        self.__prop2 = prop2
    
    def set_prop1(self, prop1: int):
        self.prop1 = prop1

    def set_prop2(self, prop2: int):
        self.__prop2 = prop2
    
    def get_prop1(self):
        return self.prop1

    def get_prop2(self):
        return self.__prop2

    

inst = A(1, "One")
print(inst.prop1, inst.get_prop2())

inst.prop1 = 2
inst.set_prop2("Second")
print(inst.get_prop1(), inst.get_prop2())

inst.set_prop1(3)
inst.set_prop2("Third")
print(inst.get_prop1(), inst.get_prop2())