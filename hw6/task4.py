# 4.Реализуйте базовый класс Car. 
#   У данного класса должны быть следующие атрибуты: 
#   speed, color, name, is_police (булево). 
#   А также методы: go, stop, turn(direction), которые должны сообщать, что машина поехала, остановилась, повернула (куда). 
#   Опишите несколько дочерних классов: 
#   TownCar, SportCar, WorkCar, PoliceCar. 
#   Добавьте в базовый класс метод show_speed, который должен показывать текущую скорость автомобиля. 
#   Для классов TownCar и WorkCar переопределите метод show_speed. 
#   При значении скорости свыше 60 (TownCar) и 40 (WorkCar) должно выводиться сообщение о превышении скорости.

class Car:
    speed: int
    color: str
    name: str
    is_police: bool

    def __init__(self, name:str, speed:int, color:str, is_police:bool = False):
        self.name = name
        self.color = color
        self.speed = speed
        self.is_police = is_police

    def go(self):
        print(f"{self._get_class_name()} {self.name}: DRIVING")

    def stop(self):
        print(f"{self._get_class_name()} {self.name}: STOPED")

    def turn(self, direction:str):
        print(f"{self._get_class_name()} {self.name}: TURNING {direction}")

    def show_speed(self) -> str:
        print(f"{self._get_class_name()} {self.name}: SPEED {self.speed}")

    def _get_class_name(self):
        return type(self).__name__

    def to_string(self):
        print("{0} [ Name: {1}, Color: {2}, {3} ]".format(self._get_class_name(), self.name, self.color, "Police" if self.is_police else "Not Police"))
        self.show_speed()

    
class TownCar(Car):

    def show_speed(self):
        if self.speed > 60:
            raise Exception(f"{self._get_class_name()} {self.name}: Speed {self.speed} is too high!")
        super().show_speed()

class WorkCar(Car):

    def show_speed(self):
        if self.speed > 40:
            raise Exception(f"{self._get_class_name()} {self.name}: Speed {self.speed} is too high!")
        super().show_speed()

class SportCar(Car):
    pass

class PoliceCar(Car):
    pass


sport_car = SportCar("Corvet", 280, "Red")
sport_car.to_string()

print()

try:
    town_car = TownCar("Ford", 60, "Blue")
    town_car.to_string()
except Exception as ex:
    print(ex)

print()

try:
    work_car = WorkCar("DAF", 80, "Green")
    work_car.to_string()
except Exception as ex:
    print(ex)