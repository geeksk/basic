# 3. Реализовать базовый класс Worker (работник), в котором определить атрибуты: 
#   name, surname, position (должность), income (доход). 
#   Последний атрибут должен быть защищенным и ссылаться на словарь, содержащий элементы: 
#   оклад и премия, например, {"wage": wage, "bonus": bonus}. 
#   Создать класс Position (должность) на базе класса Worker. 
#   В классе Position реализовать методы получения полного имени сотрудника (get_full_name) и дохода с учетом премии (get_total_income). 
#   Проверить работу примера на реальных данных (создать экземпляры класса Position, передать данные, проверить значения атрибутов, вызвать методы экземпляров).

class Worker:
    first_name: str
    last_name: str
    position: str
    _income: dict

    def __init__(self, first_name, last_name, position):
        self.first_name = first_name
        self.last_name = last_name
        self.position = position
        self._income = {"wage": 0, "bonus": 0}

    def set_income(self, wage:float, bonus:float):
        self._income["wage"] = wage
        self._income["bonus"] = bonus

    def _get_wage(self) -> float:
        return self._income["wage"]

    def _get_bonus(self) -> float:
        return self._income["bonus"]

    

class Position(Worker):
    
    def get_full_name(self):
        return "{0} {1}".format(self.first_name, self.last_name)

    def get_total_income(self):
        return self._get_wage() + self._get_bonus()



position = Position("John", "Doe", "Director")
position.set_income(100000, 20000)

print(f"Worker: {position.get_full_name()}")
print(f"Total wage: ${position.get_total_income()}")
