import sys
import os


def getArray(start, end):
    "Create an array from `start` to `end`"
    array = []
    for it in range(start, end + 1):
        array.append(it)
    return array


def inputNumber(msg, rng=None):
    "Function asks to input number until user inputs a number"
    while True:
        user_input = input(msg)             # ask user to input
        if user_input.isdigit():            # check if it is digit
            user_input = int(user_input)    # cast to int
            if rng is None:
                return user_input
            elif user_input in rng:         # if range exists checking whether range contains user input
                return user_input


def clearScreen():
    "Clear screen of terminal in Linux, or cmd window in Windows"
    # check if user uses the windows then send "cls", if the linux then "clear"
    os.system('cls' if os.name == 'nt' else 'clear')


def formatRating(rating):
    rating.sort()
    rating.reverse()
    print(f"Rating: {rating}")


def getGoods(ind):
    gname = input("Enter name of the goods: ")
    gprice = inputNumber("Enter price of the goods: ")
    gamount = inputNumber("Enter amount of the goods: ")
    gitem = input("Enter item of the goods: ")
    return (ind, {
        "name": gname,
        "price": gprice,
        "amount": gamount,
        "item": gitem
    })


def showTaskList():
    print("Select a task you want to check:"
    , ""
    , "1. Get type of each element of the list"
    , "2. Swap every two neighbor elements"
    , "3. Enter number of month and find out what season it is"
    , "4. Get splitted words of a string"
    , "5. Rating"
    , "6. Make goods list"
    , "0. Exit"
    , ""
    , sep="\n")


def exit():
    "Exit"
    sys.exit(0) # send exit


def task1():
    lst = [1, 2.4, "five", {1, 2, 3}, True, ["C", "C++"], (7, 8, 9)]
    print(f"List: {lst}", "", sep="\n")
    for el in lst:
        print(f"type of: < {el} > is {type(el)}")


def task2():
    user_input = input("Please enter any characters separated by space: ") # get input
    input_list = user_input.split(sep=" ")                                  # split input by spaces
    list_count = len(input_list)
    for i in range(0, list_count, 2):                       # go through inputs with step = 2
        if i < list_count and (i+1) < list_count:           # check if we do not go out of the array
            input_list[i], input_list[i+1] = input_list[i+1], input_list[i] # swap items
    print(input_list)


def task3():
    user_input = inputNumber("Enter number of a month: ", getArray(1, 12))  # get input
    seasons = {                                                             # create dictionary of seasons and number of months of each season
        1: {"name": "winter", "months": [12, 1, 2]},
        2: {"name": "spring", "months": [3, 4, 5]},
        3: {"name": "summer", "months": [6, 7, 8]},
        4: {"name": "fall", "months": [9, 10, 11]},
    }
    for v in seasons.values():
        if user_input in v.get("months"):           # check if season contains a user_input
            print(v.get("name"))


def task4():
    user_input = input("Enter few couples of words: ")  # get input
    words_list = user_input.split(sep=" ")              # split string by spaces and getting array of words
    ind = 1
    for w in words_list:
        if w:                           # check if word is exists (word != empty string)
            print(f"{ind}. {w[0:10]}")
            ind+=1


def task5():
    rating = [1, 2, 3]
    formatRating(rating)
    for i in range(0, 5):
        user_input = inputNumber("Enter new digit rating: ")
        rating.append(user_input)
        formatRating(rating)


def task6():
    goods = []
    for i in range(0, 3):
        goods.append(getGoods(i))   # create goods and append each to the list
        print("")                   # print empty string for pretty look
    goods_count = len(goods)        # getting amount of goods
    print(f"We have {goods_count} goods:")
    result = {"names": [], "prices": [], "amount": [], "item": []}
    for i in range(0, goods_count):
        result["names"].append(goods[i][1]["name"])
        result["prices"].append(goods[i][1]["price"])
        result["amount"].append(goods[i][1]["amount"])
        result["item"].append(goods[i][1]["item"])
    for k, v in result.items():
        print(f"{k}: {v}")


def exec_task(selected):
    tasks = {
        0: exit,
        1: task1,
        2: task2,
        3: task3,
        4: task4,
        5: task5,
        6: task6
    }
    tasks.get(selected)()   # getting name by index and execute a function


def startMenu():
    while True:
        clearScreen()
        showTaskList()
        user_choice = inputNumber("Enter a number of task: ", getArray(0, 6))
        print()
        exec_task(user_choice)
        print()
        input("Press Enter to continue...")


startMenu()
