import sys, os, math


def inputNumber(msg:str, rng:list=None, positive_only:bool=True):
    "Function asks to input number until user inputs a number"
    while True:
        user_input = input(msg)             # ask user to input
        try:
            user_input = int(user_input)
        except ValueError:
            print("Wrong enter. Try again")
            continue
        
        if positive_only and user_input < 0:
            continue

        if rng is None:
            return user_input
        elif user_input in rng:         # if range exists checking whether range contains user input
            return user_input


def clearScreen():
    "Clear screen of terminal in Linux, or cmd window in Windows"
    # check if user uses the windows then send "cls", if the linux then "clear"
    os.system('cls' if os.name == 'nt' else 'clear')


def exit():
    "Exit"
    sys.exit(0) # send exit

#==========================================
# Task 1
# 1. Реализовать функцию, принимающую два числа (позиционные аргументы) и выполняющую их деление. 
# Числа запрашивать у пользователя, предусмотреть обработку ситуации деления на ноль.

def divide(n1:int, n2:int):
    return n1 / n2


def task1():
    num1 = inputNumber("Enter first number: ", positive_only=False)
    num2 = inputNumber("Enter second number: ",  positive_only=False)
    try:
        r = divide(num1, num2)
        print(f"Result of {num1} divide by {num2} equals: {r}")
    except ZeroDivisionError:
        print("Division by zero is prohibited")

#=========================================
# Task 2
# 2. Реализовать функцию, принимающую несколько параметров, 
# описывающих данные пользователя: имя, фамилия, год рождения, город проживания, email, телефон. 
# Функция должна принимать параметры как именованные аргументы. Реализовать вывод данных о пользователе одной строкой.

#def printUser(first_name:str = "no first name", second_name:str = "no second name", dob:str = "no date of birth", city:str = "no city", email:str = "", phone_num:str = "no phone number"):
def printUser(**user_data):
    for v in user_data.values():
        print(v, end=", ")

# Lambda function
printUser2 = lambda **usr_obj: print(usr_obj, end="\n\n")


def task2():
    user_obj = {
        "name": "",
        "surname": "",
        "dob": "",
        "city": "",
        "email": "",
        "phone": ""
    }
    for k in user_obj.keys():
        user_obj[k] = input(f"Enter {k}: ")
    print()
    printUser2(**user_obj)
    printUser(**user_obj)

#=========================================
# Task 3
# 3. Реализовать функцию my_func(), которая принимает три позиционных аргумента, и возвращает сумму наибольших двух аргументов.

def sum_of_two_max(*numbers):
    nums = list(numbers)        # convert to list
    min_num = min(nums)         # find min value
    nums.remove(min_num)        # remove min value
    return sum(nums)            # return sum of remain values



def task3():
    nums = []
    for i in range(1,4):
        nums.append(inputNumber(f"Enter {i} number: "))
    r = sum_of_two_max(*nums)
    print("Result: ", r)
    

#=========================================
# Task 4
# 4. Программа принимает действительное положительное число x и целое отрицательное число y. 
# Необходимо выполнить возведение числа x в степень y. 
# Задание необходимо реализовать в виде функции my_func(x, y). 
# При решении задания необходимо обойтись без встроенной функции возведения числа в степень.

def exponentiation1(x:int, y:int):
    print(f"First variant: {x**y}")


def exponentiation2(x:int, y:int):
    print("Second variant: ", pow(x, y))


def task4():
    num1 = inputNumber("Enter number: ")
    num2 = inputNumber("Enter exponent: ", positive_only=False)
    exponentiation1(num1, num2)
    exponentiation2(num1, num2)


#=========================================
# Task 5
# 5. Программа запрашивает у пользователя строку чисел, разделенных пробелом. 
# При нажатии Enter должна выводиться сумма чисел. 
# Пользователь может продолжить ввод чисел, разделенных пробелом и снова нажать Enter. 
# Сумма вновь введенных чисел будет добавляться к уже подсчитанной сумме. 
# Но если вместо числа вводится специальный символ, выполнение программы завершается. 
# Если специальный символ введен после нескольких чисел, то вначале нужно добавить сумму этих чисел к полученной ранее сумме и после этого завершить программу.
    

def task5():
    result = 0
    while True:
        print(
            "Enter numbers separated with spaces",
            "Any non digit character will considered as end of task",
            sep="\n")
        nums = input(f"sum is [{result}] >> ").split()
        for n in nums:
            if n.isdigit():
                result += int(n)
            else:
                print(f"Result: {result}")
                return


#=========================================
# Task 6
# 6. Реализовать функцию int_func(), принимающую слово из маленьких латинских букв 
# и возвращающую его же, но с прописной первой буквой. Например, print(int_func(‘text’)) -> Text.
def processText(text:str):
    return text.title()


def task6():
    print(processText(input("Enter any text: ")))


def exec_task(selected):
    "Execute task by index"
    tasks = {
        0: exit,
        1: task1,
        2: task2,
        3: task3,
        4: task4,
        5: task5,
        6: task6
    }
    tasks.get(selected)()   # getting name by index and execute a function


def startMenu():
    while True:
        clearScreen()
        print("Select a task you want to check:"
        , ""
        , "1. Divide two number"
        , "2. User information"
        , "3. Sum of two max numbers"
        , "4. Exponentiation"
        , "5. Get sum of all entered numbers"
        , "6. Make each word in the text starts from Uppercase"
        , "0. Exit"
        , ""
        , sep="\n")
        user_choice = inputNumber("Enter a number of task: ", list(range(0,7)))
        print("\n", str("-"*10),f" TASK {user_choice} ", str("-"*10), "\n")
        exec_task(user_choice)
        print()
        input("Press Enter to continue...")


startMenu()
