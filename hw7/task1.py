# 1. Реализовать класс Matrix (матрица). 
# Обеспечить перегрузку конструктора класса (метод init()), который должен принимать данные (список списков) для формирования матрицы.

# Подсказка: матрица — система некоторых математических величин, расположенных в виде прямоугольной схемы.
# Примеры матриц: см. в методичке.
# Следующий шаг — реализовать перегрузку метода str() для вывода матрицы в привычном виде.
# Далее реализовать перегрузку метода add() для реализации операции сложения двух объектов класса Matrix (двух матриц). 
# Результатом сложения должна быть новая матрица.

# Подсказка: сложение элементов матриц выполнять поэлементно — первый элемент первой строки первой матрицы складываем с первым элементом первой строки второй матрицы и т.д.

class Matrix:
    def __init__(self, data:list):
        self.__data = data


    def __str__(self):
        result = ''
        for row in self.__data:
            for col in row:
                result = f'{result} {col}\t'
            result = f'{result}\n'
        return result


    def __add__(self, other):
        new_matrix = []
        for ir in range(0, len(self.__data)):
            new_matrix.append([])
            for ic in range(0, len(self.__data[ir])):
                new_matrix[ir].append(self.__data[ir][ic] + other[ir][ic])
        return Matrix(new_matrix)

    
    def __getitem__(self, key):
        return self.__data[key]





print('Matrix 1')
m1 = Matrix([ [1,2,3], [4,5,6], [7,8,9] ])
print(m1)

print('Matrix 2')
m2 = Matrix([ [1,2,3], [4,5,6], [7,8,9] ])
print(m2)

print('Matrix 3')
m3 = m1 + m2
print(m3)