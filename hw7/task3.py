# 3. Реализовать программу работы с органическими клетками, состоящими из ячеек. 
# Необходимо создать класс Клетка. 
# В его конструкторе инициализировать параметр, соответствующий количеству ячеек клетки (целое число). 
# В классе должны быть реализованы методы перегрузки арифметических операторов: 
#    сложение (add()), 
#    вычитание (sub()), 
#    умножение (mul()), 
#    деление (truediv()). 
# Данные методы должны применяться только к клеткам и выполнять увеличение, уменьшение, умножение и целочисленное (с округлением до целого) деление клеток, соответственно.

# Сложение. Объединение двух клеток. При этом число ячеек общей клетки должно равняться сумме ячеек исходных двух клеток.
# Вычитание. Участвуют две клетки. Операцию необходимо выполнять только если разность количества ячеек двух клеток больше нуля, иначе выводить соответствующее сообщение.
# Умножение. Создается общая клетка из двух. Число ячеек общей клетки определяется как произведение количества ячеек этих двух клеток.
# Деление. Создается общая клетка из двух. Число ячеек общей клетки определяется как целочисленное деление количества ячеек этих двух клеток.
# 
# В классе необходимо реализовать метод make_order(), принимающий экземпляр класса и количество ячеек в ряду. 
# Данный метод позволяет организовать ячейки по рядам.
# Метод должен возвращать строку вида *****\n*****\n*****..., где количество ячеек между \n равно переданному аргументу. 
# Если ячеек на формирование ряда не хватает, то в последний ряд записываются все оставшиеся.
# 
# Например, количество ячеек клетки равняется 12, количество ячеек в ряду — 5. 
# Тогда метод make_order() вернет строку: *****\n*****\n**.
# Или, количество ячеек клетки равняется 15, количество ячеек в ряду — 5. 
# Тогда метод make_order() вернет строку: *****\n*****\n*****
import math


class Cage:
    __cells: tuple

    def __init__(self, cell_count:int):
        self.__cells = tuple([i for i in range(0, cell_count)])

    @property
    def count(self) -> int:
        return len(self.__cells)

    def __str__(self):
        return ', '.join(map(lambda x: str(x), self.__cells))

    def __add__(self, other):
        return Cage(self.count + other.count)

    def __sub__(self, other):
        if self.count < other.count:
            print("Can't create cage with negative count of cells")
            return None
        return Cage(self.count - other.count)

    def __mul__(self, other):
        return Cage(self.count * other.count)

    def __truediv__(self, other):
        if other.count == 0:
            print('Division by zero is prohibited')
            return None
        return Cage(self.count // other.count)

    def make_order(self, count_in_row:int):
        if count_in_row > self.count:
            count_in_row = self.count
        start = 0
        end = count_in_row
        ind = math.ceil(self.count / count_in_row)
        result = ''
        for i in range(0, ind):
            result = f'{result}\n{self.__cells[start:end]}'
            start += count_in_row
            end += count_in_row
        print(result)

        


cage1 = Cage(4)
cage2 = Cage(4)
cage3 = cage1+cage2

print('cage1: ', cage1)
print('cage2: ', cage2)
print('cage3 (cage1 + cage2): ', cage3)
print('')

cage4 = Cage(3)
cage5 = cage3 - cage4

print('cage4: ', cage4)
print('cage5 (cage3 - cage4): ',cage5)
print('')

cage6 = cage2 * cage4

print('cage6 (cage2 * cage4): ', cage6)
print('')

cage7 = cage6 / cage4

print('cage7 (cage6 / cage4): ', cage7)
print('')

cage6.make_order(3)