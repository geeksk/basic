# 2. Реализовать проект расчета суммарного расхода ткани на производство одежды. 
# Основная сущность (класс) этого проекта — одежда, которая может иметь определенное название. 
# К типам одежды в этом проекте относятся пальто и костюм. 
# У этих типов одежды существуют параметры: размер (для пальто) и рост (для костюма). 
# Это могут быть обычные числа: V и H, соответственно.
# 
# Для определения расхода ткани по каждому типу одежды использовать формулы: 
#    для пальто (V/6.5 + 0.5), 
#    для костюма (2*H + 0.3). 
# Проверить работу этих методов на реальных данных.
# 
# Реализовать общий подсчет расхода ткани. 
# Проверить на практике полученные на этом уроке знания: реализовать абстрактные классы для основных классов проекта, проверить на практике работу декоратора @property.
from abc import ABC, abstractmethod

class Clothes(ABC):
    __name: str

    def __init__(self, name):
        self.__name = name

    @property
    def name(self):
        return f'{self.__name} ({self.__get_class_name()})'

    @abstractmethod
    def consumption(self):
        pass

    def __get_class_name(self):
        return type(self).__name__


class Coat(Clothes):
    __size: float

    @property
    def size(self):
        return self.__size

    def __init__(self, name, size):
        super().__init__(name)
        self.__size = size

    def consumption(self):
        # (V/6.5 + 0.5)
        return self.__size / 6.5 + 0.5

    def __str__(self):
        return f'{self.name} (Size: {self.__size})'


class Suit(Clothes):
    __height: float

    @property
    def height(self):
        return self.__height

    def __init__(self, name, height):
        super().__init__(name)
        self.__height = height

    def consumption(self):
        # (2*H + 0.3)
        return self.__height * 2 + 0.3

    def __str__(self):
        return f'{self.name} (Height: {self.__height})'


class ClotheComposition(Clothes):
    __clothes: list

    def __init__(self, *clothes):
        #', '.join(list(clothes))
        super().__init__('~')
        self.__clothes = list(clothes)

    def consumption(self):
        result = 0
        for it in self.__clothes:
            result += it.consumption()
        return result



coat = Coat('Georgio Armani', 6.5)
print(coat)
print('Consuption: ', coat.consumption())

print('')

suit = Suit('D & G', 0.6)
print(suit)
print('Consumption: ', suit.consumption())

print('')

composition = ClotheComposition(coat, suit)
print('Total consumption: ', composition.consumption())