import os, sys


def inputNumber(msg, rng = None):
    "Function asks to input number until user inputs a number"
    while True:
        user_input = input(msg)             # ask user to input
        if user_input.isdigit():            # check if it is digit
            user_input = int(user_input)    # cast to int
            if rng is None:                 
                return user_input
            elif user_input in rng:         # if range exists checking whether range contains user input
                return user_input


def formatNumber(number):
    "Format `number` to `two-digit number`"
    if len(str(number)) == 1:
        return "0{0}".format(number)
    else:
        return number


def secondsToTime(seconds):
    "Function returns time from `seconds`"
    m_total = seconds // 60     # getting total number of minutes
    s = seconds % 60            # getting remain seconds
    h = m_total // 60           # getting total number of hours
    m = m_total % 60            # getting remain minutes
    return "{0}:{1}:{2}".format(formatNumber(h), formatNumber(m), formatNumber(s))


def getArray(start, end):
    "Create an array from `start` to `end`"
    array = []
    for it in range(start, end):
        array.append(it)
    return array


def stringFromCharacter(character, count):
    "Getting string from repeated `character` by `count` times"
    return "{0}".format(character) * count


def getSum(number):
    "Getting sum of n + nn + nnn (`n` is `number`)"
    n2 = int(stringFromCharacter(number, 2))    # getting {nn}
    n3 = int(stringFromCharacter(number, 3))    # gettings {nnn}
    return number + n2 + n3


def clearScreen():
    "Clear screen of terminal in Linux, or cmd window in Windows"
    os.system('cls' if os.name=='nt' else 'clear') # check if user uses the windows then send "cls", if the linux then "clear"


def showTaskList():
    print("Select a task you want to check:"
    , ""
    , "1. Get time from seconds"
    , "2. Get sum of n, nn and nnn"
    , "3. Get max digit from number"
    , "4. Get business analyze"
    , "5. Calculate how many days you need"
    , "0. Exit"
    , ""
    , sep="\n")


def getMaxDigit(number):
    "Get max `digit` from specified `number`"
    n = str(number)     # cast int to string
    i = 0               # creating incremented index
    max = 0             # variable to store max value
    while i < len(n):
        d = int(n[i])   # getting digit by index
        if max < d:     # comparing
            max = d
        i+=1            # increment the index
    return max


def plusPercent(num, percent):
    num += num * (percent / 100)
    return num


def task1():
    "Task 1. Get time from seconds"
    user_seconds = inputNumber("Enter number of seconds: ")
    print(secondsToTime(user_seconds))


def task2():
    "Task 2. Get sum of n, nn and nnn"
    user_num = inputNumber("Enter number from 1 to 9: ", getArray(1,10))
    print("Sum of {0}, {1} and {2} is : {3}".format(user_num, stringFromCharacter(user_num,2), stringFromCharacter(user_num, 3), getSum(user_num)))


def task3():
    "Task 3. Getting max digit from number"
    user_input = inputNumber("Enter a number: ")
    print(getMaxDigit(user_input))


def task4():
    "Task 4."
    earned = inputNumber("How many earned: ")
    spended = inputNumber("How many spended: ")
    workers_amount = inputNumber("How many workers: ", getArray(0, 1000))
    if earned > spended:
        print("", "You are in the right way! Earned more than spended", sep="\n")
    elif earned < spended:
        print("Business is not yours! Spended more than earned")
    else:
        print("Equals, you waste your time")
    print("Each worker earned: {0}".format(earned / workers_amount))


def task5():
    "Calculate how many days you need to reach the goal"
    start = inputNumber("Km on first day: ")
    goal = inputNumber("The goal is: ")
    result = start
    day = 1
    while True:
        day += 1
        result = plusPercent(result, 10)
        if result >= goal:
            print("You will reach the goal in {0} days".format(day))
            break


def exit():
    "Exit"
    sys.exit(0)


def exec_task(selected):
    tasks = {
        0: exit,
        1: task1,
        2: task2,
        3: task3,
        4: task4,
        5: task5
    }
    tasks.get(selected)()   # getting name by index and execute a function


def startMenu():
    while True:
        clearScreen()
        showTaskList()
        user_choice = inputNumber("Enter a number of task: ", getArray(0, 6))
        print()
        exec_task(user_choice)
        print()
        input("Press Enter to continue...")


startMenu()
