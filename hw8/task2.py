# 2. Создайте собственный класс-исключение, обрабатывающий ситуацию деления на нуль. 
# Проверьте его работу на данных, вводимых пользователем. 
# При вводе пользователем нуля в качестве делителя программа должна корректно обработать эту ситуацию и не завершиться с ошибкой.

class ZeroDivisionException(Exception):
    def __str__(self):
        return f'Division by zero is prohibited'


def division(a:int, b:int):
    if float(b) == 0:
        raise ZeroDivisionException()
    return float(a) / float(b)


def processDivision():
    a = input('Input first number: ')
    b = input('Input second number: ')
    try:
        print(division(a, b))
    except ZeroDivisionException as ex:
        print(ex)


processDivision()