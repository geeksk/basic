class NotEnoughPlaceException(Exception):
    def __init__(self, placeCount, itemsCount):
        self.placeCount = placeCount
        self.itemsCount = itemsCount
    
    def __str__(self):
        return f'Not Enough Space. Total places: {self.placeCount}, Items amount: {self.itemsCount}'