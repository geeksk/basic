class InvalidDateFormatException(Exception):

    str_date: str
    
    def __init__(self, str_date):
        """string date"""
        self.str_date = str_date

    def __str__(self):
        return f'The format of specified date "{self.str_date}" is invalid'