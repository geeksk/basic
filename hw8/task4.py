# 4. Начните работу над проектом «Склад оргтехники». 
# Создайте класс, описывающий склад. 
# А также класс «Оргтехника», который будет базовым для классов-наследников. 
# Эти классы — конкретные типы оргтехники (принтер, сканер, ксерокс). 
# В базовом классе определить параметры, общие для приведенных типов. 
# В классах-наследниках реализовать параметры, уникальные для каждого типа оргтехники.

# 5. Продолжить работу над первым заданием. 
# Разработать методы, отвечающие за приём оргтехники на склад и передачу в определенное подразделение компании. 
# Для хранения данных о наименовании и количестве единиц оргтехники, а также других данных, можно использовать любую подходящую структуру, например словарь.

# 6. Продолжить работу над вторым заданием. 
# Реализуйте механизм валидации вводимых пользователем данных. 
# Например, для указания количества принтеров, отправленных на склад, нельзя использовать строковый тип данных.

# Подсказка: постарайтесь по возможности реализовать в проекте «Склад оргтехники» максимум возможностей, изученных на уроках по ООП.

from exceptions.NotEnoughPlace import NotEnoughPlaceException

class OfficeEquipment:
    brand: str
    
    def __init__(self, brand):
        self.brand = brand
    


class Printer(OfficeEquipment):
    attr1: str

    def __init__(self, brand, attr1):
        super().__init__(brand)
        self.attr1 = attr1

class Scaner(OfficeEquipment):
    attr2: str

    def __init__(self, brand, attr2):
        super().__init__(brand)
        self.attr2 = attr2

class CopyMachine(OfficeEquipment):
    attr3: str

    def __init__(self, brand, attr3):
        super().__init__(brand)
        self.attr3 = attr3


class Stock:
    _places: int
    _items: dict

    def __init__(self, places):
        self._places = places
        self._items = {}
    
    def addItem(self, item, division: str):
        if (self._places - self.itemsCount()) > 0:
            if division not in self._items.keys():
                self._items[division] = [item]
            else:
                self._items[division].append(item)
        else:
            raise NotEnoughPlaceException(self._places, self.itemsCount())

    def passItToDivision(self, division):
        if division in self._items.keys():
            self._items.pop(division)

    def itemsCount(self) -> int:
        count = 0
        for val in self._items.values():
            count += len(val)
        return count

    def prnt(self):
        print(self._items)


stock = Stock(10)
try:
    stock.addItem(Printer('samsung', 'sometext'), 'managers')
    stock.addItem(Scaner('philips', 'atttr'), 'director')
    stock.addItem(CopyMachine('xerox', 'sasd'), 'copyservice')
    stock.prnt()
except NotEnoughPlaceException as ex:
    print(ex)

stock.passItToDivision('managers')
stock.prnt()