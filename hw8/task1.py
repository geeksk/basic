# 1. Реализовать класс «Дата», функция-конструктор которого должна принимать дату в виде строки формата «день-месяц-год». 
# В рамках класса реализовать два метода. 
# Первый, с декоратором @classmethod, должен извлекать число, месяц, год и преобразовывать их тип к типу «Число». 
# Второй, с декоратором @staticmethod, должен проводить валидацию числа, месяца и года (например, месяц — от 1 до 12). 
# Проверить работу полученной структуры на реальных данных.

from exceptions.InvalidDateFormat import InvalidDateFormatException

class Date:
    init_date = ""

    def __init__(self, init_date: str):
        """params: init_date as string, format day-month-year"""
        Date.init_date = init_date


    @classmethod
    def Convert(cls):
        lst = cls.init_date.split('-')
        conv_lst = []
        for s in lst:
            conv_lst.append(int(s))
        print("Converted: ", conv_lst)


    @staticmethod
    def Validate(date_to_check: str):
        lst = Date.init_date.split('-')
        if len(lst) != 3:
            raise InvalidDateFormatException(Date.init_date)
        
        day = int(lst[0])
        month = int(lst[1])
        year = int(lst[2])
        valid = True
        
        if year < 1:
            valid = False
        
        if month not in range(1,13):
            valid = False
        
        if month in [1,3,5,7,8,10,12] and day not in range(1, 32):
            valid = False
        elif month == 2 and day not in range(1, 29):
            valid = False
        elif month in [4,6,9,11] and day not in range(1,31):
            valid = False
            
        if valid:
            return valid
        else:
            raise InvalidDateFormatException(Date.init_date)
        


def processDate(date: str):
    print('Input date: ', date)
    try:
        dd = Date(date)
        dd.Convert()
        print('Valid: ', Date.Validate(Date.init_date))
    except InvalidDateFormatException as ex:
        print(ex)



processDate('04-04-2004')
print('')
processDate('31-02-2004')
print('')
processDate('31-04-2004')
print('')
processDate('31-12-2004')
print('')
