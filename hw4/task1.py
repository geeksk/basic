# 1. Реализовать скрипт, в котором должна быть предусмотрена функция расчета заработной платы сотрудника. 
# В расчете необходимо использовать формулу: (выработка в часах*ставка в час) + премия. 
# Для выполнения расчета для конкретных значений необходимо запускать скрипт с параметрами.

from sys import argv


def calculate_salary(hours:int, hour_cost:int, bonus:int = 0):
    """Calculate salary"""
    return (hours*hour_cost) + bonus


def check_arguments():
    """Validate the input arguments"""
    if len(argv) < 3:
        print("Not enough input data to calculate salary")
        exit()
    for i in range(1, len(argv)):
        if not argv[i].isdigit() or int(argv[i]) <= 0:
            print("Some arguments are wrong")
            exit()


check_arguments()

print(calculate_salary(int(argv[1]), int(argv[2]), int(argv[3]) if len(argv) > 3 else 0))