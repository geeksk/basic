# 5. Реализовать формирование списка, используя функцию range() и возможности генератора. 
# В список должны войти четные числа от 100 до 1000 (включая границы). 
# Необходимо получить результат вычисления произведения всех элементов списка.

# Подсказка: использовать функцию reduce().

from functools import reduce

init_list = [it for it in range(100, 1001) if it % 2 == 0]

print(reduce(lambda a, b: a + b, init_list))
print(sum(init_list))