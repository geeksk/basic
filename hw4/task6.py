# 6. Реализовать два небольших скрипта:

# а) итератор, генерирующий целые числа, начиная с указанного,
# б) итератор, повторяющий элементы некоторого списка, определенного заранее.

# Подсказка: использовать функцию count() и cycle() модуля itertools. 
# Обратите внимание, что создаваемый цикл не должен быть бесконечным. Необходимо предусмотреть условие его завершения.
# Например, в первом задании выводим целые числа, начиная с 3, а при достижении числа 10 завершаем цикл. 
# Во втором также необходимо предусмотреть условие, при котором повторение элементов списка будет прекращено.

from itertools import count, cycle


def inputNumber():
    while True:
        user_input = input("Enter number: ")
        try:
            num = int(user_input)
        except ValueError:
            print("Wrong enter. Try again")
            continue
        return num


def printNumbers(first:int):
    """ a) """
    o = count(start=first)
    for n in range(first, first+10):
        print(o.__next__())


def printRecuring():
    """ b) """
    lst = list(range(10, 51, 10))
    length = len(lst)
    itr = cycle(lst)
    for i in range(0, 5):
        for j in range(0, length):
            print(itr.__next__(), end="\t")
        print()


print("а) итератор, генерирующий целые числа, начиная с указанного: ")
printNumbers(inputNumber())

print("б) итератор, повторяющий элементы некоторого списка, определенного заранее: ")
printRecuring()
