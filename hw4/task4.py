# 4. Представлен список чисел. Определить элементы списка, не имеющие повторений. 
# Сформировать итоговый массив чисел, соответствующих требованию. 
# Элементы вывести в порядке их следования в исходном списке. 
# Для выполнения задания обязательно использовать генератор.

# Пример исходного списка: [2, 2, 2, 7, 23, 1, 44, 44, 3, 2, 10, 7, 4, 11].
# Результат: [23, 1, 3, 10, 4, 11]


def get_uniq(initlist):
    for i in range(0, len(initlist)):
        if initlist[i] not in initlist[0:i] and initlist[i] not in initlist[i+1:len(initlist)]:
            yield initlist[i]


init_list = [2, 2, 2, 7, 23, 1, 44, 44, 3, 2, 10, 7, 4, 11]
print("Initial: ", end="\t")
print(init_list)

print("Result: ", end="\t")
print(list(get_uniq(init_list)))