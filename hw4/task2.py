# 2. Представлен список чисел. Необходимо вывести элементы исходного списка, значения которых больше предыдущего элемента.

# Подсказка: элементы, удовлетворяющие условию, оформить в виде списка. Для формирования списка использовать генератор.
# Пример исходного списка: [300, 2, 12, 44, 1, 1, 4, 10, 7, 1, 78, 123, 55].
# Результат: [12, 44, 4, 10, 78, 123].

init_list = [300, 2, 12, 44, 1, 1, 4, 10, 7, 1, 78, 123, 55]
print("initial: ", end="\t")
print(init_list)


def generate_new_list(initlist):
    for i in range(0, len(initlist)):
        if i > 0 and initlist[i-1] < initlist[i]:
            yield  initlist[i]


# 1.
# result_list = [it for it in generate_new_list(init_list)]

# 2.
result_list = list(generate_new_list(init_list))

print("Result: ", end="\t")
print(result_list)